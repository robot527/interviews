# Linux 开发面试题

## 1. 怎样查看与设置系统最大打开文件数？  
查看系统级最大打开文件数： `cat /proc/sys/fs/file-max`  
查看当前用户最大打开文件数：
```
ulimit -Hn #查看硬限制
ulimit -Sn #查看软限制
```

系统级的设置：
```
echo "fs.file-max = 100000" >> /etc/sysctl.conf
sysctl -p #立即生效

```

用户级的设置：
```
echo "httpd hard nofile 8192" >> /etc/security/limits.conf
echo "httpd soft nofile 4096" >> /etc/security/limits.conf
#或
ulimit -Hn 8192 #增大限制需要超级用户权限
ulimit -Sn 4096
```


## 2. 请用 shell 命令来读取文件为 12 34 56 的数据，并且输出为 56 34 12 。
```
$ cat data.txt 
12 34 56
$ awk '{for(i=NF;i>0;i--)printf($i" ");printf("\n")}' data.txt
56 34 12
# awk -F " " '{for(i=NF;i>0;i--)printf($i" ");printf("\n")}' data.txt
```

## 3. 请使用 api 写出客户端和服务器连接的过程。
服务端：

 - 第一步： 创建一个监听套接字描述符，listenfd = socket(AF_INET, SOCK_STREAM, 0);
 - 第二步： 绑定，把监听地址赋予套接字，bind(listenfd, (struct sockaddr*)&servaddr, sizeof(servaddr))
 - 第三步： 监听套接字上的连接，listen(listenfd, LISTENQ)
 - 第四步： 接受并处理客户请求，connfd = accept(listenfd, (struct sockaddr *)&cliaddr, &clilen);

客户端：

 - 第一步： 创建套接字，sockfd = socket(AF_INET, SOCK_STREAM, 0);
 - 第二步： 设置连接服务器地址结构，inet_pton(AF_INET, argv[1], &servaddr.sin_addr)
 - 第三步： 发送连接服务器请求，connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))

样例代码参见 [tcp-socket](https://bitbucket.org/robot527/unp/src/e4635a4a806bc6e527c75afe29db16cc8e842a29/tcp-socket/?at=master)

## 4. 用户态和内核态

### CPU 设计中的用户态
用户态不允许程序进行处理器中要求特权态的操作，以避免操作系统崩溃。  
每个进程都在各自的用户空间中运行，而不允许存取其他程序的用户空间。  
Intel x86 架构拥有四种级别，级别最高的是 ring 0，被 Linux 用作核心态。  
级别最低的是 ring 3，被 Linux 用作用户态。  
ring 1 和 ring 2 设计成供驱动程序使用，但一般很少使用。

### 操作系统设计中的用户态
操作系统的用户态通常是在相应的 CPU 用户态中运行代码，从而在硬件上实现对非法程序的控制。

### Linux 用户态与内核态
当进程执行系统调用而陷入内核代码中执行时，我们就称进程处于内核运行态（或简称为内核态）。  
此时处理器处于特权级最高的（0 级）内核代码中执行。  
当进程处于内核态时，执行的内核代码会使用当前进程的内核栈。每个进程都有自己的内核栈。  
当进程在执行用户自己的代码时，则称其处于用户运行态（用户态）。即此时处理器在特权级最低的（3 级）用户代码中运行。  
当正在执行用户程序而突然被中断程序中断时，此时用户程序也可以象征性地称为处于进程的内核态。  
因为中断处理程序将使用当前进程的内核栈。这与处于内核态的进程的状态有些类似。

### 区别：
内核态，运行在该模式的代码，可以无限制地对系统存储、外部设备进行访问。例如硬盘、网卡等。其所占有的 CPU 是不允许被抢占的。  
用户态，只能受限的访问内存, 且不允许访问外围设备。其所处于占有的 CPU 是可被抢占的。内核禁止此状态下的代码进行潜在危险的操作，  
比如写入系统配置文件、杀掉其他用户的进程、重启系统等。

### 为什么要有用户态和内核态？
在 CPU 的所有指令中，有一些指令是非常危险的，如果错用，将导致整个系统崩溃。  
由于需要限制不同的程序之间的访问能力，防止他们获取别的程序的内存数据，或者获取外围设备的数据，并发送到网络。  
操作系统基于安全与优雅的考虑，试图将运行在特权态的代码数量最小化。

### 用户态切换到内核态的三种方式：
系统调用、异常（比如缺页异常）、外围设备的中断，其中系统调用可以认为是用户进程主动发起的，异常和外围设备中断则是被动的。

## 5. 进程与线程
### 进程
进程是一个具有一定独立功能的程序关于某个数据集合的一次运行活动。  
一般来说，计算机系统进程由以下资源组成：

 - 与程序相关联的可执行机器码的映像。
 - 内存（通常是虚拟内存的一些区域）; 其包括可执行代码，进程特定的（输入和输出）数据，调用栈（以跟踪活动子程序和/或其他事件）以及用于保存在运行时间期间生成的中间计算数据的堆。
 - 分配给进程的资源的操作系统描述符，例如文件描述符（Unix 术语）或句柄（Windows），以及数据源和宿。
 - 安全属性，例如进程所有者和进程的权限集（允许的操作）。
 - 处理器状态（上下文），如寄存器内容和物理内存寻址。当进程执行时，状态通常存储在计算机寄存器中，否则在内存中。

### 线程
线程是进程中的一个实体，是被系统独立调度和分派的基本单位。  
线程由表示进程内执行上下文所必需的信息组成，包括线程 ID，一组寄存器值，栈，调度优先级和策略，信号掩码，errno 变量和线程特定数据。  
进程中的一切都可以在进程的线程之间共享，包括可执行程序的文本，程序的全局和堆内存，栈和文件描述符。

在一个进程中的多个线程之间，可以并发执行，甚至允许在一个进程中所有线程都能并发执行。  
同样，不同进程中的线程也能并发执行，可充分利用和发挥处理器与外围设备并行工作的能力。

### 区别
 - 地址空间和其它资源（如打开的文件）：进程间相互独立，同一进程的各线程间共享。某进程内的线程在其它进程不可见。
 - 通信：进程间通信 IPC，线程间可以直接读写进程数据段（如全局变量）来进行通信，但需要进程同步和互斥手段的辅助，以保证数据的一致性。
 - 调度和切换：同一进程中的线程上下文切换（开销小）比进程上下文切换要快得多。
 - 健壮性：线程执行的非法操作会导致整个进程崩溃；一个行为异常的线程可能中断应用程序中所有其他线程的处理。所以多进程的程序要比多线程的程序健壮。


