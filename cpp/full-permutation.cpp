#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

// 打印输出字符串的全排列

int main()
{
	string str;

	cout << "Please input a string: ";
	cin >> str;
	cout << "Full permutation of " << str << " is :" << endl;
	sort(str.begin(), str.end());
	cout << str << endl;
	while(next_permutation(str.begin(), str.end()))
	{
		cout << str << endl;
	}

	return 0;
}
