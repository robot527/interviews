#include <stdio.h>

/* sizeof 是关键字、单目运算符、长度运算符，不是函数！
When the operand is a type name, it MUST be enclosed in parenthesis. */

void pass_arr_to_func(int a[10])
{
	printf("Size of a in %s() : %lu\n", __FUNCTION__, sizeof(a));
	/* 数组a[10]的名字作为参数被函数使用时，
	a被解释为数组的首地址或者数组的指针，计算大小时在32位系统里为4。 */
}

int main(void)
{
	float f = 3.1415926;
	size_t s = sizeof f; /* 合理的使用 size_t 可以提高程序的可移植性和代码的可读性，让你的程序更高效。 */
	char *p = NULL;
	int  arr[10];

	//printf("Size of int : %lu\n", sizeof int); // error: expected expression before ‘int’
	/* 想想 sizeof int 表示什么啊？int 前面加一个关键字？类型扩展？
	明显不正确，我们可以在 int 前加 unsigned、const 等关键字但不能加 sizeof。*/
	printf("Size of int : %lu\n", sizeof(int));
	printf("Size of float : %lu\n", sizeof(float));
	printf("Size of f : %lu\n", sizeof f);
	printf("Size of size_t : %lu\n", sizeof s);
	printf("Size of 3.14 : %lu\n", sizeof 3.14);
	printf("Size of double : %lu\n", sizeof(double));
	printf("Size of pointer : %lu\n", sizeof(p));
	printf("Size of arr : %lu\n", sizeof(arr));
	printf("Size of &arr : %lu\n", sizeof(&arr));
	printf("Size of arr[11] : %lu\n", sizeof(arr[11]));
	printf("Size of &arr[0] : %lu\n", sizeof(&arr[0]));
	pass_arr_to_func(arr);

	return 0;
}
