/*
 * =====================================================================================
 *
 *       Filename:  concatenation-operator-##.c
 *
 *    Description:  预处理 连接符 双井号 "##"
 *                  merge two tokens into one when expanding macros.
 *
 *        Version:  1.0
 *        Created:  2017-03-18
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *      Reference:  https://gcc.gnu.org/onlinedocs/cpp/Concatenation.html
 *
 * =====================================================================================
 */

#include <stdio.h>

#define REG(n) register_##n

int main(void)
{
	int REG(0) = 10;
	int REG(1) = 11;
	int REG(2) = 12;
	int REG(3) = 13;

	/* Do something on these registers. */
	printf("register_%d = %#x\n", 0, REG(0));
	printf("register_%d = %#x\n", 1, REG(1));
	printf("register_%d = %#x\n", 2, REG(2));
	printf("register_%d = %#x\n", 3, REG(3));

	return 0;
}
