#! /bin/bash

# 测试 C 代码是否编译成功，并且得到的 a.out 是否执行成功。

for file in `ls *.c`; do
	echo $file
	gcc -Wall -g $file
	if test -e "a.out";then
		./a.out
		if test $? != 0;then
			exit 1
		fi
		rm a.out
	fi
	echo
done
