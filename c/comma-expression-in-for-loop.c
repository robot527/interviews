#include <stdio.h>

int main()
{
    int i;

    for( i = 0, printf("First=%d\n", i);
        //i < 10, printf("Second=%d", i);   //infinite loop
        printf("Second=%d\n", i), i < 10;
        i++, printf("Third=%d\n", i)) {
        printf("Fourth=%d\n\n", i);
    }

    printf("\n");
    for( i = 0, printf("First=%d ", i);
        i < 10, printf("Second=%d ", i); /* warning: left-hand operand of comma expression has no effect */
        i++, printf("Third=%d ", i)) {
        printf("Fourth=%d\n", i);

        if( i > 30 ) break;
    }

    return 0;
}

