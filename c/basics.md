# C 语言基础面试题

[TOC]

## 1. 下列程序在 32 位 linux 或 unix 中的结果是什么？
```
func(char *str)
{
	printf("%d ", sizeof(str));
	printf("%d ", strlen(str));
}

main()
{
	char a[] = "123456789";
	printf("%d ", sizeof(a));
	func(a);
}
输出 : 10 4 9
```

分析：sizeof 与 strlen 的区别与联系

 - sizeof 运算符的结果类型是 size_t，它在头文件中 typedef 为 unsigned int 类型。该类型保证能容纳实现所建立的最大对象的字节大小。
 - sizeof 是运算符，strlen 是函数。
 - sizeof 在编译时计算，而 strlen 在运行时计算。
 - sizeof 的参数可以是变量、数据类型、函数，strlen 的参数只能是 char *，且必须是以'\0'结尾的。
 - sizeof 后如果是数据类型必须加括弧；如果是变量名可以不加括弧，因为 sizeof 是个运算符而不是个函数。
 - 数组给 sizeof 作参数时不退化，给 strlen 作参数就退化为指针了。
 - C99 增加了将变长数组作为结构体成员的支持，使用 sizeof 计算含有变长数组的结构体的长度时不会计算变长数组的大小，见[代码](./basics-001-2.c)。


## 2. 指出程序的错误，并且写出正确的程序。
```
int delete(node * head)
{
	free(head);
	head = head->link;
	return(0);
}
```
答 : free 指针 head 后，head->link 指向“垃圾”内存，所以 head 此时也会指向“垃圾”内存。
```
//改正：
int delete(node * head)
{
	node *temp = head->link;
	free(head);
	head = temp；
	return(0);
}
```


## 3. #define MAX_NUM 10 和 const int MAX_NUM=10 的区别
答 : c 中 const 的意思是一个不能被改变的普通变量，编译器并不是把它看做一个常量，而 define 就是在编译时简单的值替换。


## 4. 实现列举满足以下条件的所有三位数，条件一：是完全平方数,条件二：有任何两个数字相同，如 144 等。
思路： 计算 10 到 32 的平方数，判断其中是三位数的数，再用除法和求余算出个位、十位、百位三个数字，通过比较找出有任何两个数字相同的数并打印输出，见[代码](./square-number.c)。

## 5. 给定两个正整数 A 和 B，问把 A 变为 B 需要改变多少二进制位（bit）？也就是说，整数 A 和 B 的二进制表示中有多少位是不同的？
思路： 先将 A 和 B 异或的结果赋值给 C，再采用移位计数或查表法求 C 的二进制数中 1 的个数，见[代码](./bit-1-count.c)。

## 6. strcpy 实现

```c
char *
strcpy(char *dest, const char *src)
{
    size_t i;

    assert(src && dest);
    for (i = 0; src[i] != '\0'; i++)
    {
        dest[i] = src[i];
    }
    dest[i] = '\0';

    return dest;
}
```

### 6.1 编码注意点

- 检查指针有效性
- 返回目的指针 dest
- 源字符串的末尾 '\0' 需要拷贝


