/*
 * =====================================================================================
 *
 *       Filename:  multi-thread.c
 *
 *    Description:  简单的多线程测试
 *
 *        Version:  0.1
 *        Created:  2017-03-17
 *       Revision:  none
 *       Compiler:  gcc
 *                  Compiles with argument " -lpthread"
 *         Author:  luoyz , NULL
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define THREAD_NUM    9

void *print_hello(void *t_num)
{
	int num;
	pthread_t tid;

	num = *(int *)t_num;
	tid = pthread_self(); // get the calling thread's ID
	// the follow line, warning: format ‘%x’ expects argument of type ‘unsigned int’, but argument 4 has type ‘pthread_t’ [-Wformat=]
	//printf("%s thread %d, ID = %#x !\n", __func__, num, tid);
	printf("%s thread %d, ID = %lu !\n", __func__, num, tid);
	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	pthread_t threads[THREAD_NUM];
	int rc;
	int t;

	for(t = 0; t < THREAD_NUM; t++)
	{
		printf("In main: creating thread %d, ", t);
		rc = pthread_create(&threads[t], NULL, print_hello, (void *)&t);
		if(rc)
		{
			printf("ERROR: return code from pthread_create() is %d\n", rc);
			exit(EXIT_FAILURE);
		}
		printf(" thread ID is : %lu\n", threads[t]);
		pthread_detach(threads[t]);// 分离线程，终止时其资源被自动释放回系统
	}
	pthread_exit(NULL);

	return 0;
}
