# 实战题


## 1. 请用两个线程交替输出A1B2C3D4...，A线程输出字母，B线程输出数字，要求A线程首先执行，B线程其次执行

实现代码参见[two-threads-and-lock](./two-threads-and-lock/)

