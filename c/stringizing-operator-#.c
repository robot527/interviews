/*
 * =====================================================================================
 *
 *       Filename:  stringizing-operator-#.c
 *
 *    Description:  预处理 字符串化运算符 井号 '#'
 *                  convert a macro argument into a string constant.
 *
 *        Version:  1.0
 *        Created:  2017-03-18
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *      Reference:  https://gcc.gnu.org/onlinedocs/cpp/Stringizing.html
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define WARN_IF(EXP) \
	do{\
		if(EXP) \
			fprintf(stderr, "Warning: " #EXP "\n");\
	} \
	while(0)

#define SQR(x) printf("The square of " #x " is %d\n", ((x) * (x)))

int main(int argc, char **argv)
{
	int num;

	if(argc < 2)
	{
		printf("Usage: %s <INTEGER>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	num = atoi(argv[1]);
	WARN_IF(num == 0);
	SQR(5);
	SQR(num);

	return 0;
}
