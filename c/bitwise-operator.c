/*
 * =====================================================================================
 *
 *       Filename:  bitwise-operator.c
 *
 *    Description:  位运算符细节
 *
 *        Version:  1.0
 *        Created:  2017-03-13
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static void swap_by_xor(int *a, int *b)
{
	*a ^= *b;
	*b ^= *a;
	*a ^= *b;
}

static void test_swap_by_xor(void)
{
	int x, y;
	long sum;
	
	srandom(time(0));
	x = random() % 401 - 200;
	y = random() % 501 - 250;
	//x = 10; y = 10;// x 没有变成0，swap_by_xor()被编译器优化了？
	sum = x + y;
	printf("Before swap: x = %d, y = %d\n", x, y);
	swap_by_xor(&x, &y);
	printf("After swap : x = %d, y = %d\n", x, y);
	if((x + y) != sum)
	{
		printf("Oops! Swap number %d, %d error!\n\n", x, y);
	}
}

int main(void)
{
	int andb = 0b11001001 & 0b10011011; // 按位与
	int orb  = 0b11001001 | 0b10011011; // 按位或
	int XOR  = 0b11001001 ^ 0b10011011; // 按位异或
	//unsigned int negationb = 0b11001001;
	unsigned char negationb = 0b11001001; // 取反，一定要注意数据类型长度！！！
	int num  = 202;
	unsigned int shiftb = 0x01<<2+3;

	if(0b10001001 == andb)
	{
		printf("(0b11001001 &\n 0b10011011) ==\n 0b10001001\n\n");
	}
	if(0b11011011 == orb)
	{
		printf("(0b11001001 |\n 0b10011011) ==\n 0b11011011\n\n");
	}
	if(0b01010010 == XOR)
	{
		printf("(0b11001001 ^\n 0b10011011) ==\n 0b01010010\n\n");
	}
	negationb = ~negationb;
	if(0b00110110 == negationb)
	{
		printf("~0b11001001 ==\n 0b00110110\n\n");
	}
	printf("~%d == %d\n\n", num, ~num); // mostly, ~202 == -203
	printf("为了最大的可移植性，您应该只使用带 无符号整数类型 的按位取反操作符。\n\n");
	if(~202 == -203)
	{
		printf("按位取反运算符的优先级比判断等于 '==' 的高！\n\n");
	}
	printf("0x01<<2+3 result: %#x\n", shiftb); // 0x20, not 0x07
	shiftb = 0x01<<2-4;// warning: left shift count is negative [enabled by default]
	// cppcheck: Shifting by a negative value is undefined behaviour
	//shiftb = 0x01 << (-2);
	printf("0x01<<2-4 result: %#x\n\n", shiftb); // 0x0
	
	test_swap_by_xor();

	return 0;
}
