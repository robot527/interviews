#include <stdio.h>

/* typedef 的真正意思是给一个已经存在的数据类型（注意：是类型不是变量）取一个别名，
而非定义一个新的数据类型。*/

#define INT32 int

typedef int int32;

int main(void)
{
	unsigned INT32 i = 10;
	//unsigned int32 j = 10; //error: expected ‘=’, ‘,’, ‘;’, ‘asm’ or ‘__attribute__’ before ‘j’
	// 用 typedef 取的类型别名不支持 **类型扩展**

	printf("Size of INT32 : %d\n", sizeof i);
	printf("Size of int32 : %d\n", sizeof(int32));

	return 0;
}
