/*
 * =====================================================================================
 *
 *       Filename:  square-number.c
 *
 *    Description:  three-digit square number which has two same digit.
 *
 *        Version:  1.0
 *        Created:  2016年11月13日 16时57分02秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *        Company:  NULL
 *
 * =====================================================================================
 */

#include <stdio.h>

int judge_three_digit(int num)
{
	int a, b, c;

	if(num < 100 || num > 999)
		return 0;
	a = num / 100;       //取百位数
	b = (num / 10) % 10; //取十位数
	c = num % 10;        //取个位数
	if(a == b || b == c || c == a)
		return 1;

	return 0;
}

int main(void)
{
	int i, sq;

	printf("Three-digit square numbers which has two same digit are: \n");
	for(i = 10; i < 40; i ++)
	{
		sq = i * i;
		if (sq > 999)
			break;
		if(judge_three_digit(sq))
			printf("%3d ", sq);
	}
	printf("\n");

	return 0;
}
