/*
 * =====================================================================================
 *
 *       Filename:  basics-001.c
 *
 *    Description:  基础部分题1, sizeof 与 strlen 的区别
 *
 *        Version:  1.0
 *        Created:  2016年11月06日 12时32分51秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *        Company:  NULL
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <string.h>

void func(char *str)
{
	printf("size of pointer: %lu\n", sizeof(str)); //计算指针的字节数
	printf("len of str: %lu\n", strlen(str)); //计算字符串的长度，不包括终止null字节('\0')
}

short foo(void)
{
	return 0;
}

int main(void)
{
	char a[] = "123456789";

	printf("size of array a: %lu\n", sizeof(a)); //字符数组的大小， char b[] = ""; 数组b的大小为1
	func(a);
	printf("size of function foo: %lu\n", sizeof(foo())); //计算函数返回值类型的大小

	return 0;
}

