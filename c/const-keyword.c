/*
 * =====================================================================================
 *
 *       Filename:  const-keyword.c
 *
 *    Description:  const 关键字验证代码
 *
 *        Version:  1.0
 *        Created:  2017-03-10 21:45:53
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *
 * =====================================================================================
 */

#include <stdio.h>

const int three = 3;

/*
 * 编译器通常不为普通 const 只读变量分配存储空间，而是将它们保存在符号表中，
 * 这使得它成为一个编译期间的值，没有了存储与读内存的操作，使得它的效率很高。
 * const 定义的只读变量从汇编的角度来看， 只是给出了对应的内存地址，
 * 而不是象 #define 一样给出的是立即数，
 * 所以 const 定义的只读变量在程序运行过程中只有一份拷贝（因为它是全局的只读变量，存放在静态区），
 * 而 #define 定义的宏常量在内存中有若干个拷贝。
 */

int main(int argc, char *argv[])
{
	int i;
	char ch[100];
	const int *p1 = (const int *)ch;     // p1 可变， p1 指向的对象不可变
	int const *p2 = (const int *)&ch[2]; // p2 可变， p2 指向的对象不可变
	int *const p3 = (int *const)&ch[10]; // p3 不可变， p3 指向的对象可变
	const int *const p4 = (const int *const)&ch[20]; // 指针 p4 和 p4 指向的对象都不可变
	const volatile int b = 3; // 两个类型限定符共存， gcc 编译通过

	//*p1 = 50; // error: assignment of read-only location ‘*p1’
	p1 = &i;
	//*p2 = 60; // error: assignment of read-only location ‘*p2’
	p2 = p1;
	printf("p2  : %p \n", p2);

	printf("*p3 : %d \n", *p3);
	*p3 = 70;
	printf("*p3 : %d \n", *p3); // *p3 : 70
	//p3 = p2; // error: assignment of read-only variable ‘p3’

	//*p4 = 80; // error: assignment of read-only location ‘*p4’
	//p4 = (const int *const)&ch[40]; // error: assignment of read-only variable ‘p4’
	printf("p4  : %p, *p4 : %d \n", p4, *p4);
	ch[20] = 2;
	printf("p4  : %p, *p4 : %d \n", p4, *p4);
	ch[21] = 1;
	printf("p4  : %p, *p4 : %d \n", p4, *p4);
	ch[22] = 0;
	printf("p4  : %p, *p4 : %d \n", p4, *p4);
	ch[23] = 0;
	printf("p4  : %p, *p4 : %d \n", p4, *p4); // BUG ??? 违背 p4 指向的对象不可变？
	/*p4 所指向的对象不可变是指不能通过 p4 的解引用来改变对象的值，并不是说对象所在的那块内存的值不可变？
	可以通过其他类型的指针访问去改写那块内存的值！*/

	i = b;
	switch(i)
	{
		case 1:
			break;
		case 2:
			break;
#if 0
		// case 关键字后面不可以跟 const 修饰的只读变量。
		case three: //error: case label does not reduce to an integer constant
			printf("case const int!\n");
			break;
#endif
		default:
			break;
	}

	return 0;
}

