#include <stdio.h>

enum color
{
    RED = 1,
    GREEN,
    BLUE,
    YELLOW = 10,
    BLACK,
    PINK
}color_val;

/* enum 变量类型可以给其中的常量符号赋值，
如果不赋值则会从被赋初值的那个常量开始依次加1，
如果都没有赋值，它们的值从0开始依次递增1。*/

int main(void)
{
    printf("RED    : %d\n", RED);
    printf("GREEN  : %d\n", GREEN);
    printf("BLUE   : %d\n", BLUE);
    printf("YELLOW : %d\n", YELLOW);
    printf("BLACK  : %d\n", BLACK);
    printf("PINK   : %d\n", PINK);

    printf("Size of color_val  : %lu\n", sizeof color_val);
    printf("Size of enum color : %lu\n", sizeof(enum color));

    return 0;
}

/* 枚举能做到事，#define 宏能不能都做到？如果能，那为什么还需要枚举？

一般在编译器里，可以调试枚举常量，但是不能调试宏常量。
一方面枚举可以集中管理数据，具有相同属性的整形数据可以使用枚举存放。
另外枚举可以实现“取值的自增”（当然也可以指定每个枚举的值），编写代码更容易，减少出错的机会，
后续维护时如果需要新增一个枚举常量则无需指定该枚举常量的值。
*/
