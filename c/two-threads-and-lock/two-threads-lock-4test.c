/* 请用两个线程交替输出 A1B2C3D4...，
 * A线程输出字母，B线程输出数字，要求A线程首先执行，B线程其次执行！ 
 * */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


#define END_CHAR  'D'

typedef struct _data_status_
{
    int  role;
    char ch;
    int  cnt_a;
    int  cnt_b;
} data_sta;

enum thr_role {
    THR_A = 0,
    THR_B,
};

pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;

void *thr_fun_a(void *arg)
{
    data_sta *ds = (data_sta *)arg;

    if (NULL == ds) {
        return (void *)-1;
    }

    while (1)
    {
        /* get lock */
        if (pthread_mutex_lock(&g_mutex) == 0) {
            ds->cnt_a++;
            if (THR_A == ds->role) {
                if (ds->ch <= END_CHAR) {
                    printf("Thread A: %c\n", ds->ch);
                    ds->ch++;
                    ds->role = THR_B;
                } else {
                    //ds->role = THR_B; //thread B already exited
                    /* unlock before exit */
                    pthread_mutex_unlock(&g_mutex);
                    pthread_exit((void *)1);
                }
            } else {
                printf("%s: not my time, %d\n", __func__, ds->cnt_a);
            }
            /* unlock */
            pthread_mutex_unlock(&g_mutex);
        }
    }

    return (void *)0;
}

void *thr_fun_b(void *arg)
{
    data_sta *ds = (data_sta *)arg;

    if (NULL == ds) {
        return (void *)-1;
    }

    while (1)
    {
        /* get lock */
        if (pthread_mutex_lock(&g_mutex) == 0) {
            ds->cnt_b++;
            if (THR_B == ds->role) {
                if (ds->ch < END_CHAR + 1) {
                    printf("Thread B: %d\n", ds->ch - 'A');
                    ds->role = THR_A;
                } else if(ds->ch == END_CHAR + 1) {
                    printf("Thread B: %d\n", ds->ch - 'A');
                    ds->role = THR_A;
                    /* unlock before exit */
                    pthread_mutex_unlock(&g_mutex);
                    pthread_exit((void *)2);
                }
            } else {
                printf("%s: not my time, %d\n", __func__, ds->cnt_b);
            }
            /* unlock */
            pthread_mutex_unlock(&g_mutex);
        }
    }

    return (void *)0;
}

int main(void)
{
    int ret;
    pthread_t tida, tidb;
    void      *tret;
    data_sta data = {THR_A, 'A'};

    ret = pthread_create(&tida, NULL, thr_fun_a, (void *)&data);
    if (ret != 0) {
        printf("create thread A failed.\n");
        exit(EXIT_FAILURE);
    }
    ret = pthread_create(&tidb, NULL, thr_fun_b, (void *)&data);
    if (ret != 0) {
        printf("create thread B failed.\n");
        exit(EXIT_FAILURE);
    }

    ret = pthread_join(tida, &tret);
    if (ret != 0) {
        printf("join with thread A failed.\n");
        exit(EXIT_FAILURE);
    }
    printf("thread A exit code %ld \n", (long)tret);
    ret = pthread_join(tidb, &tret);
    if (ret != 0) {
        printf("join with thread B failed.\n");
        exit(EXIT_FAILURE);
    }
    printf("thread B exit code %ld \n", (long)tret);

    pthread_mutex_destroy(&g_mutex);
    
    return 0;
}

