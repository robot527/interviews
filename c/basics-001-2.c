/*
 * =====================================================================================
 *
 *       Filename:  basics-001-2.c
 *
 *    Description: flexible array in struct. 
 *
 *        Version:  1.0
 *        Created:  2016年11月06日 15时59分34秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *        Company:  NULL
 *
 * =====================================================================================
 */

#include <stdio.h>

struct flexarray
{
	char val;
	int array[];  /* Flexible array member; must be last element of struct */
};

int main(int argc, char** argv)
{
	printf("sizeof (struct flexarray) = %zu\n", sizeof(struct flexarray));
	return 0;
}
