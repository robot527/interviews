/*
 * =====================================================================================
 *
 *       Filename:  big-little-endian.c
 *
 *    Description:  big-endian and little-endian
 *
 *        Version:  1.0
 *        Created:  2016年11月10日 20时07分53秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *        Company:  NULL
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <arpa/inet.h>

char *big_or_little(void)
{
	union endian
	{
		int  a;
		char b; /* read 1 byte at lowest address */
	}test;

	test.a = 1;
	if(1 == test.b)
		return "little-endian";
	else
		return "big-endian";
}

char *little_or_big(void)
{
	int  a = htonl(1); //network byte order
	char b = *(char *)&a;

	if(1 == b)
		return "little-endian";
	else
		return "big-endian";
}

int main(int argc, char **argv)
{
	printf("This processor uses %s.\n", big_or_little());

	printf("Network byte order uses %s.\n", little_or_big());

	return 0;
}

