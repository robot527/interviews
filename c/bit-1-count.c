#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int num_of_bit_one_v1(unsigned int n)
{
	int cnt = 0;
	unsigned int m = n;
	
	do
	{
		cnt += m & 0x1;
	}while(m >>= 1);
	printf("%s : number of %#x's bit 1 is: %d\n\n", __func__, n, cnt);
	
	return cnt;
}

int num_of_bit_one_v2(unsigned int n)
{
	int cnt;
	unsigned int m = n;
	
	for(cnt = 0; m; cnt++)
	{
		m &= m - 1; // 清除最低位的1
	}
	printf("%s : number of %#x's bit 1 is: %d\n\n", __func__, n, cnt);
	
	return cnt;
}

/* 查表法, 动态建表 */
int num_of_bit_one_v3(unsigned int n)
{
	static int is_init = 0;
	static unsigned char bit_num_table[256] = {0};
	int cnt = 0;
	unsigned char *p;

	if(0 == is_init)
	{
		int i;
		for(i = 0; i < 256; i++)
		{
			/* 根据非负整数的奇偶性来分析，对于任意一个不超过 unsigned int 最大值的非负整数 n，
			 * 如果n是偶数，那么n的二进制中1的个数与n/2中1的个数是相等的，因为n是由n/2左移一位而来；
			 * 如果n是奇数，那么n的二进制中1的个数是n/2中1的个数再加1，因为n等于n/2左移一位再加1。 */
			bit_num_table[i] = (i & 0x1) + bit_num_table[i / 2];
		}
		is_init = 1;
	}
	p = (unsigned char *)&n;
	cnt = bit_num_table[p[0]] + bit_num_table[p[1]]
		+ bit_num_table[p[2]] + bit_num_table[p[3]];
	printf("%s : number of %#x's bit 1 is: %d\n\n", __func__, n, cnt);

	return cnt;
}
/* 可根据 num_of_bit_one_v3() 的思路可采用静态查表法 */


/* 网上叫 平行算法 */
int num_of_bit_one_v4(unsigned int n) 
{
	unsigned int m = n;

	m = (m & 0x55555555) + ((m >> 1)  & 0x55555555);
	m = (m & 0x33333333) + ((m >> 2)  & 0x33333333);
	m = (m & 0x0f0f0f0f) + ((m >> 4)  & 0x0f0f0f0f);
	m = (m & 0x00ff00ff) + ((m >> 8)  & 0x00ff00ff);
	m = (m & 0x0000ffff) + ((m >> 16) & 0x0000ffff);
	printf("%s : number of %#x's bit 1 is: %u\n\n", __func__, n, m);

	return m; 
}

/* 整数 a 和 b 的二进制表示中有多少位是不同 */
int num_of_diff_bits(unsigned int a, unsigned int b)
{
	unsigned int m = a ^ b;

	return num_of_bit_one_v3(m);
}

int main(void)
{
	unsigned int n;

	num_of_bit_one_v1(7);
	num_of_bit_one_v1(0xfe);
	srandom(time(0));
	n = random();
	num_of_bit_one_v1(n);
	num_of_bit_one_v2(n);
	num_of_bit_one_v3(n);
	num_of_bit_one_v3(n / 2);
	num_of_bit_one_v4(n);

	printf("整数 %d 和 %d 的二进制表示中有 %d 位不同。\n",
			7, 8, num_of_diff_bits(7, 8));

	return 0;
}

/* 更多版本参见：
http://wenku.baidu.com/view/7e127d563c1ec5da50e2701d.html
https://www.cnblogs.com/graphics/archive/2010/06/21/1752421.html
*/
