# Git 合并多个 commit

## 1. 查看提交日志

确定一个需要保持不变的最新提交的 commit ID，比如 535fd9e，使用命令 `git rebase -i 535fd9e`  

![log](./git-rebase-step-0.jpg)

## 2. 开始 rebase

![step-1](./git-rebase-step-1.jpg)

## 3. 编辑指令并保存

![step-2](./git-rebase-step-2.jpg)

## 4. 编辑提交消息

![step-3](./git-rebase-step-3.jpg)

## 5. 保存消息且提交

![step-4](./git-rebase-step-4.jpg)

完成后的效果：

![result](./git-rebase-squash-ok.jpg)

