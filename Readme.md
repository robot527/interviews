# 面试准备和经验

学习记录面试题。

总结面试经验。

## 分类
 - [C语言](./c/basics.md)
 - [Linux](./linux/Readme.md)
 - [GDB](./gdb/Readme.md)
 - [Python](./python/key-basis.md)

持续积累，加油！
