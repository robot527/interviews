#! /usr/bin/env python
# -*- coding: UTF-8 -*-

a = [] #全局变量

def fun(a):
    print "Enter function, id of a:     ", id(a)
    #a[0] = 1 #IndexError: list assignment index out of range
    a.append(1)
    print "After list append, id of a:  ", id(a)
    a = ['a', 'b', 'c']
    print "Assignment a as:", a
    print "After assignment, id of a:   ", id(a)

print "Outside function, id of a:   ", id(a)
fun(a)

print a  # [1]

#函数内的引用指向的是 可变对象，对它的操作就同定位了指针地址一样，是在所指对象的内存里进行修改。
#如果在函数中重新绑定引用，外部范围将不会知道它，并且完成后，外部引用仍将指向原始对象。
# https://stackoverflow.com/questions/986006/how-do-i-pass-a-variable-by-reference
