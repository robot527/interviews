#Python 中很基础但重要的知识
--------------------------------------

## Evaluation order
Python evaluates expressions from left to right.  
Notice that while evaluating an assignment, the right-hand side is evaluated before the left-hand side.  
Python 对表达式从左到右求值。注意，在计算赋值时，右侧在左侧之前被求值。

在下面几行中，表达式将以其后缀的数字顺序进行计算：
```
expr1, expr2, expr3, expr4
(expr1, expr2, expr3, expr4)
{expr1: expr2, expr3: expr4}
expr1 + expr2 * (expr3 - expr4)
expr1(expr2, expr3, *expr4, **expr5)

expr3, expr4 = expr1, expr2
```

## Assignment statements
[赋值语句][1]用于将名字（重新）绑定到值，并修改可变对象的属性或条目：
```
assignment_stmt ::=  (target_list "=")+ (expression_list | yield_expression)
target_list     ::=  target ("," target)* [","]
target          ::=  identifier
                     | "(" target_list ")"
                     | "[" [target_list] "]"
                     | attributeref
                     | subscription
                     | slicing
```

赋值语句对表达式列表(记住这可以是单个表达式或逗号分隔的列表，后者产生一个元组)求值，并将单个结果对象从左到右分配给每个目标列表。

赋值根据目标（列表）的形式递归地定义。当目标是可变对象(属性引用、[subscription][2] 或切片)的一部分时，可变对象必须最终执行赋值并决定其有效性，并且如果赋值是不可接受的，则可能引发异常。  
The rules observed by various types and the exceptions raised are given with the definition of the object types (参见章节 [The standard type hierarchy][3]).

将对象赋值到目标列表的递归地定义如下：

 - 如果目标列表是单个目标：将对象赋值给该目标。
 - 如果目标列表是逗号分隔的目标列表：对象必须是具有与目标列表中的目标相同数量的可迭代的条目，并且条目被从左到右赋值到相应的目标。

将对象赋值给单个目标的递归定义如下：

 - 如果目标是标识符（名字）：
    * 如果名字未出现在当前代码块中的全局语句中：名字被绑定到当前局部命名空间中的对象。
    * 否则：名字被绑定到当前全局命名空间中的对象。
 - 如果目标是括在括号中或方括号中的目标列表：对象必须是具有与目标列表中的目标相同数量的可迭代的条目，并且条目被从左到右赋值到相应的目标。
 - 如果目标是属性引用：将对引用中的主表达式求值。它应该产生一个具有可赋值属性的对象; 如果不是这种情况，则引发 TypeError。然后要求该对象将所分配的对象赋值给给定属性; 如果它不能执行赋值，则引发异常(通常是但不一定是 AttributeError)。
 - 如果目标是下标表达式 (subscription)：将对引用中的主表达式求值。它应该产生一个可变序列对象（例如列表）或映射对象（例如字典）。接下来，下标表达式被求值。
 - 如果目标是切片：将对引用中的主表达式求值。它应该产生一个可变的序列对象（如列表）。所分配的对象应该是相同类型的序列对象。接下来，下界和上界的表达式被求值，只要它们存在；默认值为零和序列的长度。边界应该求值为（小）整数。如果任一边界为负数，则将序列长度加给它。所得到的边界被夹在零和序列长度之间，包括端点。最后，序列对象被要求用赋值序列的条目替换切片。切片的长度可能与指定的序列的长度不同，因此如果对象允许，则改变目标序列的长度。

**警告**：虽然赋值的定义意味着左侧和右侧之间的重叠是“安全的”(例如 a，b = b，a 交换两个变量)，但是在被赋值变量的集合中的重叠是不安全的！  
例如，以下程序打印 [0, 2]:
```
x = [0, 1]
i = 0
i, x[i] = 1, 2 ##参看前面所述的求值顺序
print x
```



--------------------------------------------------------------------------------
译者：[robot527](https://github.com/robot527)

[1]:https://docs.python.org/2/reference/simple_stmts.html#assignment-statements
[2]:https://docs.python.org/2/reference/expressions.html#grammar-token-subscription
[3]:https://docs.python.org/2/reference/datamodel.html#types


