#! /usr/bin/env python
# -*- coding: UTF-8 -*-

a = 1 #全局变量

def fun(a):
    print "Enter function, id of a:        ", id(a) #用内建函数 id() 查看对象的标识(内存地址)
    a = 2 #将局部变量 a 绑定到对象 '2' 上
    print "After assignment, id of a and 2:   ", id(a), id(2) #局部变量 a 的标识

print "Outside function, id of a and 1:", id(a), id(1)
fun(a)
print "After exec function, id of a and 1:", id(a), id(1)

print a  # 1

#当一个引用传递给函数的时候，函数自动复制一份引用。
#全局的 a 指向了一个 不可变对象，函数内的 a 被赋值（改变引用）时，外面的 a 仍然不变。
